AngularJS Sublime Snippets
Author: Eduardo Delai <edelaicorrea@gmail.com>
Version: 0.0.3

########## Intro ##########
This is an unofficial fork from Johnpapa's angular-styleguide
(github.com/johnpapa/angular-styleguide), focused on AngularJS Sublime Snippets.

####### Requirements ######
You will need the following:
  - Sublime Text 3 (prefered)
  or
  - Sublime Text 2

####### Setting up ########
Windows & Linux:
  Open menu: Preferences > Browse Packages. Clone this repository under User folder.
Mac:
  Open menu: Sublime Text 2 > Preferences > Browser Packages. Clone this repository under User folder.

##### How to extend it ####
Refer to Sublime Snippets sintax on http://sublimetext.info/docs/en/extensibility/snippets.html.

########### Tip ###########
Check JohnPapa's angular-styleguide (github.com/johnpapa/angular-styleguide) for more IDEs snippets.
